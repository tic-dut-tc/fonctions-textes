# Fonctions Textes

+ Pour regrouper (ou concaténer) du texte, le plus simple c'est d'utiliser le symbole ``&``( ou ``=CONCATENER(cell1;cell2.....)``)
+ La fonction **CONCAT** permet de regrouper du texte en selectionnant une plage de données : ``CONCAT(Plage de cellules à concaténer)``
+ La fonction **JOINDRE.TEXTE** permet aussi la concaténation de plusieurs cellules : ``=JOINDRE.TEXTE(Délimiteur; Prise en compte des cellules vides (VRAI ou FAUX); Plage de cellules à concaténer)``
+ La fonction **GAUCHE** récupère les n caractères qui commencent votre chaîne de caractères. Son écriture est la suivante : ``=GAUCHE(Chaîne de caractères; n caractères)``
+ La fonction **DROITE** récupère les n caractères qui terminent votre chaîne de caractères. Son écriture est la suivante : ``=DROITE(Chaîne de caractères; n caractères)``
+ La fonction **STXT** extrait x caractères à l'intérieur d'une chaîne de caractères : ``=STXT(Chaîne de caractères;position du début de la sélection; nombre de caractères à sélectionner)``
+ La fonction **NBCAR** (ce qui signifie nombre de caractères) compte le nombre de signes (lettres, nombres, ponctuations, espaces) qui composent votre chaîne de caractères : ``=NBCAR(Chaîne de caractères)``
+ La fonction **CHERCHE** permet de trouver la position d'un ou plusieurs caractères spécifiques dans une chaîne de caractères: ``=CHERCHE(La chaîne de caractères à rechercher; La cellule de recherche; [optionnel] La position de départ de recherche)``
+ La fonction **TROUVE** est utilisée pour renvoyer le numéro de la position d'un caractère spécifique ou d'une sous-chaîne dans une chaîne de texte: ``=TROUVE(texte_a_trouver; texte_principal; [debut_de_recherche])``
+ La fonction **NOMPROPRE** transforme la première lettre de chaque mot en majuscule et les autres lettres en minuscules: ``=NOMPROPRE(texte)``
+ La fonction **MAJUSCULE** convertit une chaîne de caractères en majuscules: ``=MAJUSCULE(texte)``
+ La fonction **SIERREUR** vérifie si la valeur entrée est une erreur, elle retourne ensuite cette même valeur s'il n'y a aucune erreur ou une autre valeur définie en cas d'erreur: ``=SIERREUR(valeur, valeur_si_erreur)``


# ![logo](https://gitlab.com/tic-dut-tc/les-exercices/raw/master/Excel.png) Les bases :

+ ## [**CONCATENER**](fichiers/texte1.xlsx)
+ ## [**CONCATENER : A vous de jouer**](fichiers/texte2.xlsx)
+ ## [**MAJUSCULE - GAUCHE - STXT**](fichiers/texte3.xlsx)
+ ## [**MAJUSCULE - GAUCHE - STXT : A vous de jouer**](fichiers/texte4.xlsx)
+ ## [**Synthèse 1**](fichiers/texte5.xlsx)
+ ## [**Synthèse 2**](fichiers/texte6.xlsx)
+ ## [**Synthèse 3**](fichiers/texte7.xlsx)


# ![logo](https://gitlab.com/tic-dut-tc/les-exercices/raw/master/Excel.png) Les exercices d'application :
+ ## [**Traiter les e-mails**](fichiers/e-mail.xlsx)
+ ## [**Trop de points**](fichiers/TropDePoints.xlsx)



# ![logo](images/gestiondeprojet.png) Réaliser un document Excel que vous nommerez [``secu.xlsx``](fichiers/secu.xlsx), qui vous permettra :

+ **de calculer le numéro de sécurité sociale d'un individu.**
+ **d'afficher les données accessibles à partir d'un numéro de sécurité sociale.**

![secu](fichiers/Satellite.gif)

> Clé NIR = 97 - ( ( Valeur numérique du NIR ) modulo 97 ) : Le modulo est l'opérateur qui calcule le reste de la division Euclidienne.


**``MOD(nombre, diviseur)``**

Renvoie le reste de la division de l’argument nombre par l’argument diviseur. Le résultat est du même signe que diviseur.
Syntaxe

**La syntaxe de la fonction MOD comporte les arguments suivants :**

+ **nombre** : Paramètre obligatoire. Représente le nombre à diviser pour obtenir le reste.
+ **diviseur** : Paramètre obligatoire. Représente le nombre par lequel vous souhaitez diviser le nombre.

